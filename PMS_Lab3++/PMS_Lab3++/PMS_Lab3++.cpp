#include "stdafx.h"

#include <Windows.h>
#include <gl/gl.h>
#include "GL/glut.h"

#define _USE_MATH_DEFINES
#include <cmath>
#include <vector>
#include <string>
#include <iostream>
#include <stdlib.h>

using namespace std;

float areaWidth = 200;
float areaHeight = 200;
int windowWidth, windowHeight;
float k = 0;
float scale = 1.0;
float scaleStep;
float angle = 0;
float PolygonVertices[6][2];
int currentVertex = 0;

void rotateFunction(int value);

void init(void)
{
	glClearColor(1, 1, 1, 1);
	glMatrixMode(GL_PROJECTION);
	gluOrtho2D(-areaWidth, areaWidth, 0.0, -areaHeight);
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	glEnable(GL_LINE_SMOOTH);
	glHint(GL_POLYGON_SMOOTH_HINT, GL_NICEST);
}

void winReshapeFcn(GLint width, GLint height)
{
	windowWidth = width;
	windowHeight = height;

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluOrtho2D(-areaWidth, areaWidth, -areaHeight, areaHeight);
	glViewport(0, 0, width, height);
	glClear(GL_COLOR_BUFFER_BIT);    // �������� ����
}

void drawShapes(void)
{
	glClear(GL_COLOR_BUFFER_BIT);

	glMatrixMode(GL_MODELVIEW);

	glLoadIdentity();
	if (angle <= 360)
	{
		glRotatef(angle, 0, 0, 1);
	}
	else
	{
		angle = 0;
		if (currentVertex < 6) currentVertex++;
		else currentVertex = 0;
	}
	glScalef(scale, scale, scale);
	glTranslatef(PolygonVertices[currentVertex][0], PolygonVertices[currentVertex][1], 0);

	glColor3b(0, 0, 0);
	glLineWidth(1);
	glBegin(GL_POLYGON);
		{
			glVertex2f(PolygonVertices[0][0], PolygonVertices[0][1]);
			glVertex2f(PolygonVertices[1][0], PolygonVertices[1][1]);
			glVertex2f(PolygonVertices[2][0], PolygonVertices[2][1]);
			glVertex2f(PolygonVertices[3][0], PolygonVertices[3][1]);
			glVertex2f(PolygonVertices[4][0], PolygonVertices[4][1]);
			glVertex2f(PolygonVertices[5][0], PolygonVertices[5][1]);
		}
	glEnd();
	//�����������


	glFlush();
}

void nextRotation(int value)
{
	glMatrixMode(GL_MODELVIEW);
	glPopMatrix();
	glLoadIdentity();

	glTranslatef(PolygonVertices[currentVertex][0], PolygonVertices[currentVertex][1], 0);
	glutTimerFunc(25, rotateFunction, 0);

	
}

void rotateFunction(int value)
{
	if (value <= 360)
	{
		value++;
		if (value <= 180)
		{
			scale -= scaleStep;
		}
		else
		{
			scale += scaleStep;
		}
	}
	else value = 0;
	angle++;
	glutTimerFunc(20, rotateFunction, value);
}

void idle()
{
	glutPostRedisplay();
}

int main(int argc, char** argv)
{
	cout << "k = ";
	cin >> k;
 
	float scalePercentage = (100 - k);
	float scaleK = scalePercentage / 100;
	scaleStep = (1 - scaleK) / 180;

	PolygonVertices[0][0] = -60.0; PolygonVertices[0][1] = -30.0;
	PolygonVertices[1][0] = 0.0; PolygonVertices[1][1] = -60.0;
	PolygonVertices[2][0] = 60.0; PolygonVertices[2][1] = -30.0;
	PolygonVertices[3][0] = 60.0; PolygonVertices[3][1] = 30.0;
	PolygonVertices[4][0] = 0.0; PolygonVertices[4][1] = 60.0;
	PolygonVertices[5][0] = -60.0; PolygonVertices[5][1] = 30.0;

	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_SINGLE | GLUT_RGB | GLUT_MULTISAMPLE);
	glutInitWindowPosition(0, 0);
	glutInitWindowSize(400, 400);
	glutCreateWindow("����������� ������ �3");
	init();

	glTranslatef(PolygonVertices[currentVertex][0], PolygonVertices[currentVertex][1], 0);
	glutTimerFunc(250, rotateFunction, 0);

	glutIdleFunc(idle);
	glutDisplayFunc(drawShapes);
	glutReshapeFunc(winReshapeFcn);
	glutMainLoop();
}

