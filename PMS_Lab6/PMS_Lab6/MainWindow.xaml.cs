﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace PMS_Lab6
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            DoubleAnimation buttonAnimation = new DoubleAnimation();
            buttonAnimation.From = 1;
            buttonAnimation.To = 25;
            buttonAnimation.Duration = TimeSpan.FromSeconds(1);
            buttonAnimation.RepeatBehavior = RepeatBehavior.Forever;
            buttonAction.BeginAnimation(FontSizeProperty, buttonAnimation);
        }

        private void cbYandex_MouseEnter(object sender, MouseEventArgs e)
        {

        }

        private void cbYandex_MouseLeave(object sender, MouseEventArgs e)
        {

        }

        private void cbYandex_Click(object sender, RoutedEventArgs e)
        {

        }

        private void DockPanel_MouseEnter(object sender, MouseEventArgs e)
        {
            cbYandex.IsEnabled = false;
            DoubleAnimation disappearanceAnimation = new DoubleAnimation();
            disappearanceAnimation.From = 1;
            disappearanceAnimation.To = 0;
            disappearanceAnimation.Duration = TimeSpan.FromSeconds(0.5);
            cbYandex.BeginAnimation(OpacityProperty, disappearanceAnimation);
        }

        private void DockPanel_MouseLeave(object sender, MouseEventArgs e)
        {
            cbYandex.IsEnabled = true;
            DoubleAnimation appearanceAnimation = new DoubleAnimation();
            appearanceAnimation.From = 0;
            appearanceAnimation.To = 1;
            appearanceAnimation.Duration = TimeSpan.FromSeconds(0.25);
            cbYandex.BeginAnimation(OpacityProperty, appearanceAnimation);
        }

        private void buttonAction_Click(object sender, RoutedEventArgs e)
        {
            if ((bool)cbYandex.IsChecked)
            {
                labelOutput.Content = "Дякуємо, що\nАБСОЛЮТНО ДОБРОВІЛЬНО\nвстановили наші продукти!\n(Варіант 4, анімований текст)";
            }
            else
            {
                labelOutput.Content = "Як вам це взагалі вдалося?";
            }
            DoubleAnimation labelAnimation = new DoubleAnimation();
            labelAnimation.From = 0;
            labelAnimation.To = 1;
            labelAnimation.Duration = TimeSpan.FromSeconds(1);
            labelAnimation.RepeatBehavior = RepeatBehavior.Forever;
            labelOutput.BeginAnimation(OpacityProperty, labelAnimation);
            cbYandex.Visibility = Visibility.Hidden;
            buttonAction.Visibility = Visibility.Hidden;
        }

        private void buttonAction_MouseEnter(object sender, MouseEventArgs e)
        {

        }

        private void buttonAction_MouseLeave(object sender, MouseEventArgs e)
        {
        }
    }
}
