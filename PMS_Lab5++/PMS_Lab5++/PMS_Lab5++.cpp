// PMS_Lab5++.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"

#include <Windows.h>
#include <gl/gl.h>
#include "GL/glut.h"
#include "Textures.h"

GLfloat xRotated, yRotated, zRotated;
GLdouble radius = 1;

float ambientLight[4] = { 0.6, 0.6, 0.6, 1.0 };
float Lt0amb[4] = { 0.8, 0.8, 0.16, 1.0 };
float Lt0diff[4] = { 1.0, 1.0, 0.2, 1.0 };
float Lt0spec[4] = { 1.0, 1.0, 0.2, 1.0 };
float Lt0pos[4] = { 1.7*radius, 0.0, 0.0, 1.0 };

float Lt1amb[4] = { 0.0, 0.0, 0.5, 1.0 };
float Lt1diff[4] = { 0.0, 0.0, 0.5, 1.0 };
float Lt1spec[4] = { 0.0, 0.0, 1.0, 1.0 };
float Lt1pos[4] = { 0.0, 1.2*radius, 0.0, 1.0 };

// Material values
float Noemit[4] = { 0.0, 0.0, 0.0, 1.0 };
float Matspec[4] = { 1.0, 1.0, 1.0, 1.0 };
float Matnonspec[4] = { 0.4, 0.05, 0.4, 1.0 };
float Matshiny = 16.0;

GLuint sphereTexture;

void init(void)
{
	
	glEnable(GL_LIGHTING);      
	glEnable(GL_LIGHT0);      
	glEnable(GL_LIGHT1);
	glLightModelfv(GL_LIGHT_MODEL_AMBIENT, ambientLight);   

	glLightfv(GL_LIGHT0, GL_AMBIENT, Lt0amb);
	glLightfv(GL_LIGHT0, GL_DIFFUSE, Lt0diff);
	glLightfv(GL_LIGHT0, GL_SPECULAR, Lt0spec);

	glLightfv(GL_LIGHT1, GL_AMBIENT, Lt1amb);
	glLightfv(GL_LIGHT1, GL_DIFFUSE, Lt1diff);
	glLightfv(GL_LIGHT1, GL_SPECULAR, Lt1spec);

	sphereTexture = loadTexture("010.tga");
}

void redisplayFunc(void)
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	glMatrixMode(GL_MODELVIEW);

	glLightModeli(GL_LIGHT_MODEL_LOCAL_VIEWER, GL_TRUE);

	glMaterialfv(GL_FRONT_AND_BACK, GL_AMBIENT_AND_DIFFUSE, Noemit);
	glMaterialfv(GL_FRONT_AND_BACK, GL_SPECULAR, Noemit);
	glMaterialf(GL_FRONT_AND_BACK, GL_SHININESS, 0.0);

	glPushMatrix();
	glTranslatef(Lt0pos[0], Lt0pos[1], Lt0pos[2]);
	glMaterialfv(GL_FRONT_AND_BACK, GL_EMISSION, Lt0spec);
	glutSolidSphere(0.2, 5, 5);
	glPopMatrix();
	glEnable(GL_LIGHT0);
	glLightfv(GL_LIGHT0, GL_POSITION, Lt0pos);

	glPushMatrix();
	glTranslatef(Lt1pos[0], Lt1pos[1], Lt1pos[2]);
	glMaterialfv(GL_FRONT_AND_BACK, GL_EMISSION, Lt1spec);
	glutSolidSphere(0.2, 5, 5);
	glPopMatrix();
	glEnable(GL_LIGHT1);
	glLightfv(GL_LIGHT1, GL_POSITION, Lt1pos);

	glLoadIdentity();

	glColor3f(1.0, 1.0, 1.0);

	glMaterialfv(GL_FRONT_AND_BACK, GL_AMBIENT_AND_DIFFUSE, Matnonspec);
	glMaterialfv(GL_FRONT_AND_BACK, GL_SPECULAR, Matspec);
	glMaterialf(GL_FRONT_AND_BACK, GL_SHININESS, Matshiny);
	glMaterialfv(GL_FRONT_AND_BACK, GL_EMISSION, Noemit);

	GLUquadricObj *quadratic = gluNewQuadric();
	gluQuadricNormals(quadratic, GLU_SMOOTH);
	gluQuadricTexture(quadratic, GL_TRUE);

	glEnable(GL_TEXTURE_2D);


	glLoadIdentity();

	glBindTexture(GL_TEXTURE_2D, sphereTexture);

	glTranslatef(0.0, 0.0, -5.0);
	glRotatef(xRotated, 1.0, 0.0, 0.0);
	glRotatef(yRotated, 0.0, 1.0, 0.0);
	glRotatef(zRotated, 0.0, 0.0, 1.0);
//	glScalef(1.0, 1.0, 1.0);
	gluSphere(quadratic, radius, 100, 100);

	glLoadIdentity();

	glFlush();

}

void reshapeFunc(int x, int y)
{
	if (y == 0 || x == 0) return;
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	//Angle of view:40 degrees
	//Near clipping plane distance: 0.5
	//Far clipping plane distance: 20.0

	gluPerspective(40.0, (GLdouble)x / (GLdouble)y, 0.5, 20.0);
	glMatrixMode(GL_MODELVIEW);
	glViewport(0, 0, x, y); 
}

void idleFunc(void)
{

	zRotated += 0.05;

	redisplayFunc();
}


int main(int argc, char **argv)
{
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_SINGLE | GLUT_RGB | GLUT_MULTISAMPLE | GLUT_DEPTH);
	glutInitWindowSize(400, 350);
	glutCreateWindow("����������� �5");
	init();

	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	xRotated = yRotated = zRotated = 0.0;
	xRotated = -110.0;
	yRotated = 15.0;
	glClearColor(0.0, 0.0, 0.0, 0.0);
	glutDisplayFunc(redisplayFunc);
	glutReshapeFunc(reshapeFunc);
	glutIdleFunc(idleFunc);
	glutMainLoop();
	return 0;
} 