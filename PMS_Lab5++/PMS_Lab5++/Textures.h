#ifndef TEXTURES_H_INCLUDED
#define TEXTURES_H_INCLUDED

#include <Windows.h>
#include <gl/GL.h>
#include <gl/GLU.h>
#include <string>
#include "TGA.h"
#include <stdlib.h>

GLuint loadTexture(std::string filename);

extern GLuint defaultTexture;

#endif // TEXTURES_H_INCLUDED
